import { Box } from "@mui/system";
import axios from "axios";
import React, { useEffect, useState } from "react";
import Appbar from "../Components/Appbar";
import LeftList from "./LeftList";
import RightSide from "./RightSide";

const Homepage = () => {
    const [posts, setPosts] = useState()

    const handleFetch = async() => {
        try {
            const data = await axios.get('http://localhost:3004/postgenerated')
            setPosts(data.data)
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        handleFetch()
    }, [])
    console.log(posts)
 return (
  <div>
   <Appbar />
   <Box
    sx={{
     width: "80%",
     margin: "0 auto",
     display: "flex",
     maxWidth: '1200px',
     height: '100%'
    }}>
    <Box
    className='right'
     style={{
      width: "75%",
      display: 'flex',
      flexWrap: "wrap",
      gap: '10px',
      padding: '25px',
      justifyContent: 'center',
      alignItems: 'center'
     }}>
        {posts?.map((post) => {
            return(

     <RightSide key={post.post_id} post={post}/>
            )
        })}
    </Box>
    <Box
    className='left'
     style={{
      width: "25%",
      padding: '25px',
     }}>
     <LeftList/>
    </Box>
   </Box>
  </div>
 );
};

export default Homepage;
