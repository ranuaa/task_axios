import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import MenuItem from "@mui/material/MenuItem";
import CameraAltIcon from "@mui/icons-material/CameraAlt";
import "../Fonts.css";

function Appbar() {
 const [anchorElNav, setAnchorElNav] = React.useState(null);

 const handleOpenNavMenu = (event) => {
  setAnchorElNav(event.currentTarget);
 };

 const handleCloseNavMenu = () => {
  setAnchorElNav(null);
 };

 return (
  <AppBar position="static">
   <Container maxWidth="xl">
    <Toolbar disableGutters>
     <CameraAltIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
     <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
      <IconButton
       size="large"
       aria-label="account of current user"
       aria-controls="menu-appbar"
       aria-haspopup="true"
       onClick={handleOpenNavMenu}
       color="inherit">
       <MenuIcon />
      </IconButton>
      <Menu
       id="menu-appbar"
       anchorEl={anchorElNav}
       anchorOrigin={{
        vertical: "bottom",
        horizontal: "left",
       }}
       keepMounted
       transformOrigin={{
        vertical: "top",
        horizontal: "left",
       }}
       open={Boolean(anchorElNav)}
       onClose={handleCloseNavMenu}
       sx={{
        display: { xs: "block", md: "none" },
       }}>
       <MenuItem onClick={handleCloseNavMenu}>
        <Typography textAlign="center">Blog App</Typography>
       </MenuItem>
       <MenuItem onClick={handleCloseNavMenu}>
        <Typography textAlign="center">ABOUT</Typography>
       </MenuItem>
       <MenuItem onClick={handleCloseNavMenu}>
        <Typography textAlign="center">MEMORY</Typography>
       </MenuItem>
       <MenuItem onClick={handleCloseNavMenu}>
        <Typography textAlign="center">LOGIN</Typography>
       </MenuItem>
      </Menu>
     </Box>
     <CameraAltIcon sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} />
     <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
      <Button
       className="sono"
       onClick={handleCloseNavMenu}
       sx={{ my: 2, color: "white", display: "block" }}>
       BLOG APP
      </Button>
      <Button
       className="sono"
       onClick={handleCloseNavMenu}
       sx={{ my: 2, color: "white", display: "block" }}>
       ABOUT
      </Button>
      <Button
       className="sono"
       onClick={handleCloseNavMenu}
       sx={{ my: 2, color: "white", display: "block" }}>
       MEMORY
      </Button>
      <Box sx={{ flexGrow: 1 }} />
      <Button
       className="sono"
       onClick={handleCloseNavMenu}
       sx={{ my: 2, color: "white", display: "block" }}>
       Login
      </Button>
     </Box>
    </Toolbar>
   </Container>
  </AppBar>
 );
}

export default Appbar;
